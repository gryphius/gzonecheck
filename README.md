gzonecheck is a simple tool to monitor DNS zones on authoritative servers.

## checks

 * zone-serial: zone serial in synch
 * connectivity-udp: DNS Server responds over UDP 
 * zone-dnssec-remaining-percentage: DNSSEC signature of SOA : remaining life time > 50% of total signature lifetime

Arguments:
(brainstorm)
```
--zones-from : filename where to read zones, whitespace separated. default '-' for stdin
--servers : list of ip adresses to check, if not given, uses recursive resolver to get all nameserver ips for each zone. can also contain hostnames, on which cases the recursive resolver is used to find all A/AAAA records
--recursive-resolver: default 9.9.9.9, used to get list of nameserver ips if --server-ips is missing
--run-tests: default 'all', list of tests to run, comma separated
--skip-tests: default empty, list of test names to skip
--debug : print debug log

--qps : max queries per second per server ips
--concurrency: default 50, how many parallel threads 

Test specific:
--zone-remaining-dnssec-percentage : default 50, minimum remaining percentage of total dnssec lifetime
```

Output:
(brainstorm)
```
{"zone":"wgwh.ch",
 "all-tests-ok": true/false,
 "failed-tests": ['zone-serial']
 "human-readable-errors": []
 "zone-dnssec-remaining-percentage":57, # minimum value of all nameserver
 "zone-serial": 230, #maximum value seen
 "server-ips":{
  '1.2.3.4': { 
     "zone-serial": 229,
     "zone-dnssec-remaining-percentage":57,
  }, 
  '4.5.6.7': {
     "zone-serial": 230,
     "zone-dnssec-remaining-percentage":59,
  }, 
  '8.9.9.9': {
     "udp-connectivity": false,
  }, 
  }
 } 
}
```
## Usage examples:

echo 'wgwh.ch' | gzonecheck --
`pdnsutil list-all-zones | gzonecheck` :  get nameserver ips automatically from a recursive resolver, perform checks

